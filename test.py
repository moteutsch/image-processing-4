import matplotlib.pyplot as plt
from scipy.misc import imread
from skimage.color import rgb2gray
import sol4

im = imread('./external/backyard1.jpg')
im_gray = rgb2gray(im)
print(sol4.harris_corner_detector(im_gray))
