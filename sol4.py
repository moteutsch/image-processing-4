# TODO:
# TODO: + Make sure all types of ndarrays are coorrect

import numpy as np
import scipy
import scipy.ndimage
import math
import copy
import matplotlib.pyplot as plt
import os
from scipy.ndimage.interpolation import map_coordinates
import random
import sys

import sol4_utils as utils
import sol4_add

X_DIRECTION = 0
Y_DIRECTION = 1

HARRIS_CORNER_K = 0.04

DESCRIPTOR_RAIDUS = 3

SPREAD_OUT_CORNERS_M = 7
SPREAD_OUT_CORNERS_N = 7
SPREAD_OUT_CORNERS_RADIUS = 10

MATCH_FEATURES_MINIMAL_SCORE = 0.7

# TODO: What is a good value here?
RANSAC_NUM_ITERS = 10000

# TODO: What is a good value here?
DISPLAY_MATCHES_INLINER_TOL = 6

### 3.1

def derivative(im, direction):
    if direction == X_DIRECTION:
        filter = np.array([[1, 0, 1]])
    elif direction == Y_DIRECTION:
        filter = np.array([[1, 0, 1]]).T
    else:
        raise ValueError('Invalid direction for derivative')

    return scipy.ndimage.filters.convolve(
        im, filter, mode='constant', cval=0.0)

def derivative_x(im):
    return derivative(im, X_DIRECTION)

def derivative_y(im):
    return derivative(im, Y_DIRECTION)


def harris_corner_detector(gray_im):
    """
    :param gray_im: grayscale image to find key points inside
    :return: An array with shape (N,2) of [x,y] key point locations in gray_im
    """

    # Remember that differentiation is done left to right!
    der_xx = derivative_x(derivative_x(gray_im))
    der_yy = derivative_y(derivative_y(gray_im))
    der_xy = derivative_y(derivative_x(gray_im))
    der_yx = derivative_x(derivative_y(gray_im))

    det = der_xx * der_yy - der_xy * der_yx
    trace = der_xx + der_yy

    corner_response = det - HARRIS_CORNER_K * trace
    # Binary array containing only local maxima
    non_maximum_suppressed = sol4_add.non_maximum_suppression(corner_response)

    #plt.figure()
    #plt.imshow(non_maximum_suppressed, cmap=plt.get_cmap('gray'))
    #plt.show()

    return np.argwhere(non_maximum_suppressed > 0)[:,::-1]

def cross_pyramid_level_coordinate_transform(level_from, level_to, point_array):
    # See scipy.ndimage.map_coordinates
    #pts = copy.deepcopy(point_array)
    const = 2 ** (level_from - level_to)
    #pts[:,0] *= const
    #pts[:,1] *= const
    #return pts
    #print(type(point_array), point_array.shape, point_array.dtype)
    return point_array * const
    #return const * point_array[0,:], const * point_array[1,:]


def find_features(gaussian_pyramid):

    # pyramid = utils.build_gaussian_pyramid(im_gray, max_levels=3,
    # filter_size=3)[0]

    harris_pts = sol4_add.spread_out_corners(gaussian_pyramid[0],
            SPREAD_OUT_CORNERS_M, SPREAD_OUT_CORNERS_N,
            SPREAD_OUT_CORNERS_RADIUS)
    #for i in range(3):
        #plt.figure()
        #show_im_gray(gaussian_pyramid[i])
    #plt.show()
    descs = sample_descriptor(gaussian_pyramid[2], np.array(harris_pts),
                              DESCRIPTOR_RAIDUS)
    #print('find_featuers', 'shape', descs.shape)
    return harris_pts, descs


def sample_descriptor(im_gray, positions, desc_rad):
    """
    :param im_gray: Grayscale image
    :param positions: An array of shape (N,2) of [x,y] positions to sample descriptors
    :param desc_rad: "Radius" of descriptors to compute (see "return")
    :return: desc - A 3D array with shape (K,K,N) containing the ith
    descriptor at desc(:,:i). The per-descriptor dimensions KxK are related to
    the desc_rad argument as follows: K = 1 + 2*desc_rad
    """

    # TODO: What is correct filter size?
    # pyramid = utils.build_gaussian_pyramid(im_gray, max_levels=3,
                                           #filter_size=3)[0]

    N = positions.shape[0]
    K = 1 + 2*desc_rad

    x = np.arange(-DESCRIPTOR_RAIDUS, DESCRIPTOR_RAIDUS + 1, 1)
    y = np.arange(-DESCRIPTOR_RAIDUS, DESCRIPTOR_RAIDUS + 1, 1)

    # This returns python array (size 2) each containing numpy array of KxK
    parts = np.meshgrid(x, y)

    # Flip parts (y -> x, x -> y)
    # Flip dimensions so that if pos in dim3_positions, pos[0] and pos[1] are
    #  flipped.
    parts = np.array(parts)[::-1,...]

    # Dupicate N-times
    parts_N = np.repeat(parts[np.newaxis,:], N, 0)

    # Map coordinates to location in 3rd-level pyramid
    dim3_positions = cross_pyramid_level_coordinate_transform(1, 3, positions)

    feature_coordinates = (dim3_positions.T + parts_N.T).T

    # TODO: NOTE: There LOTS (most) of constant patches that are returned.
    # TODO: NOTE: This is highly unlikely if operating correctly. Probably we
    # TODO: NOTE: Are sampling from outside the image.

    # NOTE: This loop is okay according to forum post.
    patches = []
    for single_feature_coordinates in feature_coordinates:
        patches.append(map_coordinates(im_gray,
            #single_feature_coordinates.reshape(2, K ** 2), order=1))
            single_feature_coordinates, order=1))

    patches = np.array(patches)
    #print('Patch shape', patches[0].shape)

    # NOTE: Returned shape is flipped!

    delta_patches = patches - patches.mean(axis=(1, 2), keepdims=True)
    norms = np.linalg.norm(delta_patches, 2, axis=(1, 2), keepdims=True)
    #print('sample desc', norms.shape, delta_patches.shape)
    delta_patches_f = delta_patches[norms.reshape(N) != 0]
    norms_f = norms[norms.reshape(N) != 0]
    # print(delta_patches_f.shape, delta_patches.shape, norms_f.shape)
    #print(np.nonzero(norms == 0))
    #np.delete(delta_patches, np.nonzero(norms == 0))
    #np.delete(norms, np.nonzero(norms == 0))
    normalized_patches = delta_patches_f / norms_f
    res = normalized_patches.reshape(K, K, normalized_patches.shape[0])
    # print('Result shape', res.shape)
    return res


def match_features(descs1, descs2, min_score):
    """
    :param descs1: A feature decriptor array with shape (K,K,N_1)
    :param descs2: A feature decriptor array with shape (K,K,N_2)
    :param min_score: Minimal match score required for features to match
    :return: (match_ind1, match_ind2) where match_indN is array with
        shape (M,) and dtype *int* of matching indices in descN
    """
    # print(descs1.shape, descs2.shape)

    K = descs1.shape[0]
    # print('K', K)
    N_1 = descs1.shape[2]
    N_2 = descs2.shape[2]
    # print('N_1, N_2', N_1, N_2)

    # Note that this matrix (of co-variances) is symmetric by properties of
    # co-variance/dot-product.
    scores = np.dot(
        descs1.T.reshape(N_1, K ** 2),
        descs2.reshape((K ** 2, N_2))
    )
    # print('Scores SHAPE:')
    # print(scores.shape)

    # TODO: NOTE: Should this be N_1 x N_2, or N_2 x N_1?
    bool_axis_0 = np.zeros((N_1, N_2))
    bool_axis_1 = np.zeros((N_1, N_2))

    top_axis_0 = scores.argsort(0)[-2:,:]
    top_axis_1 = scores.argsort(1)[:,-2:]

    # print(scores[0], scores[:,0])
    # print(np.nanmax(scores[:,0]))
    # print(top_axis_0[0], top_axis_1[:,0])

    # print('Ranges')
    # print(np.count_nonzero(~np.isnan(scores)))
    # print(scores.max())
    # print(scores.min())
    #
    # print('Axes')
    # print(top_axis_0.shape)
    # print(top_axis_1.shape)

    for i in range(N_2):
        # if i == 1:
            # print('HERE WE GO')
            # print(top_axis_0[:,i])
        bool_axis_0[top_axis_0[:,i], i] = 1
    for i in range(N_1):
        bool_axis_1[i, top_axis_1[i,:]] = 1

    # print(np.argwhere(bool_axis_0 > 0), np.argwhere(bool_axis_1 > 0))

    matches = np.argwhere(bool_axis_0 * bool_axis_1 * (scores > min_score))
    if len(matches) == 0:
        return np.array([], dtype=np.int), np.array([], np.int)

    # TODO: Not sure if this is correct. Would tihs contain duplicates?
    # TODO: Also, would, is this returned in correct order?
    return matches[:,0], matches[:,1]


def apply_homography(pos1, H12):
    """
    Given a point (x_1, y_1) in coordinate system 1 and a homography matrix
    H_{1,2}, then apply_homography will transform this point to (x_2, y_2) in
    coordinate system 2 (in way described in PDF)

    :param pos1: An array with shape (N,2) of [w,y] point coordinates
    :param H12: A 3x3 homography matrix
    :return: pos2 - An array with the same shape as pos1 with [x,y] point
                    coordinates in image i+1 obtained from tranforming pos1
                    using H_{1,2}
    """
     # Add ones to "pos1" array.
    N = pos1.shape[0]
    ones = np.ones((N, 1))
    pos1_3dim = np.concatenate((pos1, ones), axis=1)

    # Apply homography matrix. Need to transpose once for matrix
    # multplication to be legal (in terms of dimnesions lining up) and
    # transpose again to preserve original orientation
    pos2_3dim = np.dot(H12, pos1_3dim.T).T
    # We want to return a list of [x,y] coordinates (without z) and divide
    # them by z coordinate
    return pos2_3dim[:,:2] / pos2_3dim[:,2].reshape(N, 1)


def ransac_homography(pos1, pos2, num_iters, inlier_tol):
    """

    :param pos1: Array with shape (N,2) containing n rows of
            [x, y] coordinates of matched points
    :param pos2: Array with shape (N,2) containing n rows of
            [x, y] coordinates of matched points
    :param num_iters: Number of RANSAC iterators to perform
    :param inlier_tol: Inlier tolerance threshold
    :return: H12, inliners:
        1. A 3x3 normalizer homogrpahy matrix
        2. An array with shape (S,) where S is the number of inliers containing
           the indices in pos1/pos2 of the maximal set of inlier matches found
    """
    N = pos1.shape[0]

    best_inliner_indexes = None
    best_inliner_count = None

    # Iterate
    for i in range(num_iters):
        random_coords_indexes = random.sample(range(N), 4)
        # print(pos1, random_coords_indexes)
        pos1_sampled = pos1[random_coords_indexes]
        pos2_sampled = pos2[random_coords_indexes]

        # print(pos2_sampled.shape, pos1_sampled.shape)

        # Homography can now be either a 3x3 matrix or "None" if unstable
        # solution
        homography = sol4_add.least_squares_homography(pos1_sampled, pos2_sampled)
        if homography is None:
            # We ignore this iteration
            continue

        # The transformed set P^'_2 (as in PDF)
        pos2_tag = apply_homography(pos1, homography)

        # Euclidean norm error between homography (pos2_tag) and target image
        # (pos2). Shape of this will be (N,).
        norm2_error = np.sum(pos2 - pos2_tag, axis=1) ** 2

        # Not actually "indexes". Array of shape (N,) with True for inliner
        # and false for outlier
        current_inliner_indexes = norm2_error < inlier_tol
        current_inliner_count = np.count_nonzero(current_inliner_indexes)

        # If this set of inliners is better than the best so far, make this
        # the new best.
        if best_inliner_indexes is None \
            or current_inliner_count > best_inliner_count:

            best_inliner_indexes = current_inliner_indexes
            best_inliner_count = current_inliner_count

    # Now that we have the homography that maximizes the number of inliners,
    # we recompute the homography on inliners only

    # The "best_inliner_indexes" is an array of booleans, hence suitable for
    # indexing. "pos1_sampled' will now be of shape (S,2) where S is # of
    # True in "best_inliner_indexes".
    pos1_sampled = pos1[best_inliner_indexes,:]
    pos2_sampled = pos2[best_inliner_indexes,:]

    homography = sol4_add.least_squares_homography(pos1_sampled, pos2_sampled)
    if homography is None:
        print('CRITICAL ERROR: The best homography could not be computed')
        sys.exit(1)

    # np.nonzero converts the array of bools to an array of indexes of
    # "True", but is contained in wrapper tuple.
    return homography, np.nonzero(best_inliner_indexes)[0]


def display_matches(gray_im1, gray_im2, pos1, pos2, inliers):
    """
    :param gray_im1: Grayscale image
    :param gray_im2: Grayscale image
    :param pos1: Array of shape (N,2) containing [x,y] coordinates of matched
                 points in im1 and im2 (i.e. the match of ith coordinate is
                 pos[i,:] in im1 and pos2[i,:] in im2.
    :param pos2: As above. (N is same in both.)
    :param inliers: An array with shape (S,) of inliner matches. (See
                    "return" value of ransac_homography.
    """

    # TODO: Is this correct index?
    first_image_width = gray_im1.shape[1]
    N = pos1.shape[0]

    combined_image = np.concatenate((gray_im1, gray_im2), axis=1)
    plt.figure()
    show_im_gray(combined_image)
    # print('Shape in display_matches:')
    # print(pos1[inliners, :].shape)
    # print(pos1[inliners, :].shape)
    # print(pos1[inliners, :].shape)

    outliers = np.setdiff1d(np.arange(N), inliers)

    pos1_inliers = pos1[inliers,:]
    pos2_inliers = pos2[inliers,:]

    pos1_outliers = pos1[outliers,:]
    pos2_outliers = pos2[outliers,:]

# + np.array([0, N])
    plt.plot(
        (pos1_outliers[:, 0], pos2_outliers[:, 0] + first_image_width),
        (pos1_outliers[:, 1], pos2_outliers[:, 1]),
        mfc='r', color='b', lw=0.5, ms=7, marker='.', zorder=2
    )
    plt.plot(
        (pos1_inliers[:, 0], pos2_inliers[:, 0] + first_image_width),
        (pos1_inliers[:, 1], pos2_inliers[:, 1]),
        mfc='r', color='y', lw=0.5, ms=7, marker='.', zorder=3
    )
    # for i in range(N):
    #     if i in inliners:
    #         pass
    #         # Draw line between two points (yellow)
    #     else:
    #         pass
    #         # Draw line between two points (yellow)
    plt.show()


def accumulate_homographies(H_successive, m):
    """
    :param H_successive: A list of M-1 3x3 homography matrices where
         H_successive[i] is a homography that transforms points from coordinate
         system i to coordinate system i+1. (I.e., H_successive[i] = H_{i,i+1}
    :param m: Index of the coordinate system we would like to accumulate the
              given homographies toward
    :return: H2m - A list of M 3x3 homography matrices, where H2m[i]
            transforms points from coordinate system i to system m.
    """
    M = len(H_successive) + 1
    H2m = []
    for i in range(m):
        res = np.eye(3, 3)
        for j in range(i, m):
            res = np.dot(H_successive[j], res)
        H2m.append(res)

    H2m.append(np.eye(3, 3))

    for i in range(m + 1, M):
        res = np.eye(3, 3)
        for j in range(m, i):
            res = np.dot(H_successive[j], res)
        H2m.append(np.linalg.inv(res))

    # Make numpy array
    H2m = np.array(H2m)
    return H2m / (H2m[:,2,2]).reshape(M, 1, 1)


def render_panorama(ims, Hs):
    """
    :param ims: A (Python) list of gray-scale images
    :param Hs: A (Python) list of 3x3 homography matrices. Hs[i] is a
               homography that transforms points from the coordinate system
               of ims[i] to the coordinate system of the panorama.
    :return: panorama - A grayscale panorama image composed of vertical
        strips, backwarped using homographies from Hs, one from every image
        in ims.
    """
    Hs = np.array(Hs)
    M = len(ims)

    # Find corners of all images. The 4 corners of image "i" will be in
    # corners[i] (i=0,...,M-1). Similarly find the images of all images in
    # the main coordinate system (I_{pano}).
    corner_ims = []
    center_coordinates_of_images = []
    center_ims = []
    for i in range(M):
        corners = np.array([
            [0, 0], [0, ims[i].shape[0]],
            [ims[i].shape[1], 0], [ims[i].shape[1], ims[i].shape[0]]
        ])
        corner_ims.append(apply_homography(corners, Hs[i]))

        center_coordinate = [ims[i].shape[0] // 2, ims[i].shape[1] // 2]
        center_ims.append(
            apply_homography(np.array([center_coordinate]), Hs[i])[0])
        center_coordinates_of_images.append(center_coordinate)

    # Now that we have the images of the corners of each pictures under their
    # respective transformation (i.e., we have the positions of all corners in
    # I_{pano}.
    corner_ims = np.array(corner_ims)

    # We round down and up for min and max respectively to ensure that image is
    # large enough.
    x_min, x_max = np.floor(corner_ims[:,:,0].min()), np.ceil(corner_ims[:, :, 0].max())
    y_min, y_max = np.floor(corner_ims[:,:,1].min()), np.ceil(corner_ims[:, :, 1].max())

    all_frame_heights = np.ceil(y_max - y_min)

    result_panorama = None
    #strips = []
    total_widths = 0
    for i in range(M):
        # If we're on last run, the right-hand boundary (x-coordinate) will be
        # the edge of the image
        right_x_boundary = (x_max - x_min + 1 - total_widths) if i == M - 1 \
                else (center_ims[i][0] + center_ims[i + 1][0]) // 2

        # Pair of 1-tuples. THe first contains x-coordinates, the right
        # contains y-coordinates.
        current_strip = np.meshgrid(
            np.arange(all_frame_heights),
            np.arange(total_widths, total_widths + right_x_boundary)
        )
        current_strip_xs = current_strip[0].flatten() \
                           #- center_ims[center_image_coord][0]
        current_strip_ys = current_strip[1].flatten()

        x_y_strip_pairs = np.concatenate(
            (current_strip_xs.reshape((current_strip_xs.shape[0], 1)),
            current_strip_ys.reshape((current_strip_ys.shape[0], 1))),
            axis=1
        )
        if result_panorama is not None:
            plt.figure()
            show_im_gray(result_panorama)
            plt.show()

        frame_i_coordinates = apply_homography(
                x_y_strip_pairs, np.linalg.inv(Hs[i]))
        frame_i_values = map_coordinates(ims[i], frame_i_coordinates.T, order=1)
        strip_values = frame_i_values.reshape((all_frame_heights, right_x_boundary))
        #strips.append(strip_values)
        if result_panorama is None:
            result_panorama = strip_values
        else:
            result_panorama = np.concatenate((result_panorama, strip_values), axis=1)

        total_widths += right_x_boundary

    return result_panorama






    # 1. Build strips
    # 2. For each strip s[i]
    # 2.1. Inverse homography: H_i^-1(s[i])
    # 2.2. Map coordiantes


#### TESTING CODE

def test_display_matches():
    im_gray1 = utils.read_image('./external/backyard1.jpg', 1)
    im_gray2 = utils.read_image('./external/backyard1.jpg', 1)

    pyramid1 = utils.build_gaussian_pyramid(im_gray1, max_levels=3,
                                            filter_size=7)[0]
    pyramid2 = utils.build_gaussian_pyramid(im_gray2, max_levels=3,
                                            filter_size=7)[0]

    pts1, descs1 = find_features(pyramid1)
    pts2, descs2 = find_features(pyramid2)

    features = match_features(descs1, descs2, MATCH_FEATURES_MINIMAL_SCORE)

    feature_pts1 = pts1[features[0]]
    feature_pts2 = pts2[features[1]]

    homography, inliners = ransac_homography(
        feature_pts1, feature_pts2,
        RANSAC_NUM_ITERS, DISPLAY_MATCHES_INLINER_TOL
    )

    display_matches(im_gray1, im_gray2, feature_pts1, feature_pts2, inliners)


def plot_pt(x, y, size=2, pt_percent=None):
    # http://jsfiddle.net/W8Tby/

    if pt_percent is None:
        color = 'green'
    else:
        i = (pt_percent * 255)
        r = round(math.sin(0.024 * i + 0) * 127 + 128)
        g = round(math.sin(0.024 * i + 2) * 127 + 128)
        b = round(math.sin(0.024 * i + 4) * 127 + 128)
        color = (r / 255, g / 255, b / 255)

    plt.plot(x, y, color=color, linestyle='dashed', marker='o',
         markersize=size, lw=.5)

def show_im_gray(gray_im):
    plt.imshow(gray_im, cmap = plt.get_cmap('gray'), vmin = 0, vmax = 1)

def visualize_harris(gray_im):
    #harris_pts = harris_corner_detector(gray_im)
    harris_pts = sol4_add.spread_out_corners(gray_im,
         SPREAD_OUT_CORNERS_M, SPREAD_OUT_CORNERS_N,
         SPREAD_OUT_CORNERS_RADIUS)
    show_im_with_pts(gray_im, harris_pts)


def show_im_with_pts(gray_im, pts, show=True):
    plt.figure()
    plt.imshow(gray_im, cmap = plt.get_cmap('gray'), vmin = 0, vmax = 1)
    for pt_index in range(len(pts)):
        plot_pt(pts[pt_index][0], pts[pt_index][1], 3, pt_index / len(pts))
    if show:
        plt.show()


def harris_example():
    #im = utils.read_image('./external/backyard1.jpg', 1)
    im = utils.read_image('./img/small-image.png', 1)
    visualize_harris(im)


def test_descriptor():
    #im = utils.read_image('./external/backyard1.jpg', 1)
    im = utils.read_image('./img/small-image.png', 1)
    harris_pts = sol4_add.spread_out_corners(im,
             SPREAD_OUT_CORNERS_M, SPREAD_OUT_CORNERS_N,
             SPREAD_OUT_CORNERS_RADIUS)

    plt.figure()
    show_im_gray(im)

    pts = harris_pts[[100,300,500]]

    # Plot feature point
    for pt in pts:
        plot_pt(pt[0], pt[1], 5)

    # Show descriptor
    descs = sample_descriptor(im, np.array(pts), DESCRIPTOR_RAIDUS)
    descs = descs.reshape(3, 7, 7)
    print(descs.reshape(3, 7, 7))
    for desc in descs:
        plt.figure()
        show_im_gray(desc)
    plt.show()


def test_find_features():
    #im_gray = utils.read_image('./external/backyard1.jpg', 1)
    im_gray = utils.read_image('./img/small-image.png', 1)
    pyramid = utils.build_gaussian_pyramid(im_gray, max_levels=3,
                                           filter_size=3)[0]
    pts, descs = find_features(pyramid)
    print(pts.shape, descs.shape)


def test_match_features():
    im_gray1 = utils.read_image('./external/backyard1.jpg', 1)
    #im_gray1 = utils.read_image('./img/small-image.png', 1)
    im_gray2 = utils.read_image('./external/backyard2.jpg', 1)
    #im_gray2 = utils.read_image('./img/small-image.png', 1)

    #visualize_harris(im_gray1)
    #visualize_harris(im_gray2)

    pyramid1 = utils.build_gaussian_pyramid(im_gray1, max_levels=3,
                                           filter_size=3)[0]
    pyramid2 = utils.build_gaussian_pyramid(im_gray2, max_levels=3,
                                           filter_size=3)[0]
    # TODO: Returned shape by find_features is wrong! (Flipped.)
    # TODO: NOTE: this will not affect the result of match_features,
    # TODO: NOTE: by symmetry of dot-product.
    pts1, descs1 = find_features(pyramid1)
    pts2, descs2 = find_features(pyramid2)
    print('What is the shape??', descs1.shape)

    # Note that "features[0][i]" and "features[1][i]" are "matching features"
    # for all i
    features = match_features(descs1, descs2, MATCH_FEATURES_MINIMAL_SCORE)
    print(features)

    how_many_pts = 300
    show_im_with_pts(im_gray1, pts1[features[0][-how_many_pts:]], show=False)
    show_im_with_pts(im_gray2, pts2[features[1][-how_many_pts:]])


def test_accumulate_homographies():
    ms = (np.arange(5).reshape(5, 1, 1) + 1) \
         * np.array([np.eye(3), np.eye(3), np.eye(3), np.eye(3), np.eye(3), ])
    print('Matrices')
    print(ms)
    res = accumulate_homographies(ms, 2)
    print('Result')
    print(res)


if __name__ == '__main__':
    #harris_example()
    #test_descriptor()
    #test_find_features()
    #test_match_features()
    #test_display_matches()
    test_accumulate_homographies()
    #render_panorama()
