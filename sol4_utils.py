import numpy as np
import scipy
import scipy.ndimage
import math
import copy
import matplotlib.pyplot as plt
import os



################# read_image

from scipy.misc import imread, imshow, imsave
from skimage.color import rgb2gray

REPRESENTATION_GRAYSCALE = 1
REPRESENTATION_RGB = 2

REPRESENTATIONS = [REPRESENTATION_GRAYSCALE, REPRESENTATION_RGB]

def _guard_representation(representation):
    if representation not in REPRESENTATIONS:
        raise ValueError


def read_image(filename, representation):
    """
    Reads image from file into "representation" and returns it as image
    np.ndarray of type np.float32 with intensities in [0, 1]
    """

    _guard_representation(representation)
    if not isinstance(filename, str):
        print(str(filename), type(filename))
        raise ValueError

    im = imread(filename)
    if representation == REPRESENTATION_GRAYSCALE:
        if im.dtype == np.uint8:
            im = im.astype(np.float32) / 255.0
        return rgb2gray(im).astype(np.float32)
    return im.astype(np.float32) / 255


##############################################################





def make_1d_gaussian(size):
    """
    Make normalized 1D gaussian-approximation filter of size "size"
    :return: np.array
    """
    g = np.array([1, 1])
    while g.size < size:
        g = np.convolve(g, np.array([1, 1]))
    return (g / (2 ** (size - 1))).reshape(1, g.size)


def convolve_2d_with_filter(image, filter):
    """
    Helper function that performs 2D convolution by repeatedly doing 1D
    convolution using "scipy.ndimage.filters.convolve".

    Does not mutate "image".

    :param image: ndarray representing image
    :param filter: 1-dimensional ndarray
    :return The resulting, convolved image
    """

    new_image = scipy.ndimage.filters.convolve(image, filter,
            mode='constant', cval=0.0)
    new_image = scipy.ndimage.filters.convolve(new_image, filter.T,
            mode='constant', cval=0.0)
    return new_image


def build_gaussian_pyramid(im, max_levels, filter_size):
    """
    Args:
        im: np.array: A gray-scale image with values in [0, 1] (e.g., output of
            ex1's "read_image" with representation = 1
        max_levels: Max. levels in pyramid
        filter_size: Size of gaussian filter to use

    Returns: gaussian_pyramid (a python array of images), gaussian_filter_vec
    """

    gaussian = make_1d_gaussian(filter_size)
    if max_levels == 0:
        return [], gaussian

    # Create a deep-copy of "im" that we will manipulate when creating levels.
    current_image = copy.deepcopy(im)

    pyramid = [current_image]

    # Number of levels (not including the first level, which is the starting
    # image itself) to create. Note that the smallest level image's
    # minimum dimension (i.e., min(height, width)) should not be (strictly)
    # less than 16.
    levels = min(
        int(math.floor(math.log(min(im.shape) / 16, 2))),
        max_levels - 1      # "-1" because we've already put in the starting
                            # image
    )

    # Create levels of the pyramid
    for i in range(levels):
        # Create a deep-copy of "im" that we will manipulate when creating
        # levels.
        current_image = reduce(current_image, gaussian)
        pyramid.append(current_image)

    return pyramid, gaussian


def reduce(image, filter):
    """
    Reduce image using filter. Returns *new*, deep-copied image. Does not
    mutate "image".
    """
    return copy.deepcopy(convolve_2d_with_filter(image, filter)[::2,::2])


def expand(image, filter):
    """
    Expands "image" using filter. Returns *new* image which is twice the size
    of "image". Does not mutate "image".
    """

    # Create a new image "new_image" twice the size of "image", zero-padded
    # so that zeros appear in the odd places (e.g., new_image[1][1] = 0).
    new_image = np.zeros((image.shape[0] * 2, image.shape[1] * 2), np.float32)
    new_image[::2,::2] = image

    # Blur using given filter
    return convolve_2d_with_filter(new_image, 2 * filter)


def repeated_expand(n_times, image, filter):
    expanded = image
    for i in range(n_times):
        expanded = expand(expanded, filter)
    return expanded


def build_laplacian_pyramid(im, max_levels, filter_size):
    """
    Args:
        im: np.array: A gray-scale image with values in [0, 1] (e.g., output of
            ex1's "read_image" with representation = 1
        max_levels: Max. levels in pyramid
        filter_size: Size of gaussian filter to use

    Returns: gaussian_pyramid (a python array of images), gaussian_filter_vec
    """

    g_pyramid, filter = build_gaussian_pyramid(im, max_levels, filter_size)
    l_pyramid = []

    for i in range(len(g_pyramid) - 1):
        l_pyramid.append(g_pyramid[i] - expand(g_pyramid[i + 1], filter))

    # Add last gaussian level to laplacian pyramid. (This is the only level
    # in the laplacian pyramid which ought not be the difference between two
    # gaussians.
    l_pyramid.append(g_pyramid[-1])
    return l_pyramid, filter


def laplacian_to_image(l_pyramid, filter, coefficients):
    image = coefficients[0] * l_pyramid[0]

    for i in range(1, len(l_pyramid)):
        image += coefficients[i] * repeated_expand(i, l_pyramid[i], filter)

    return image


def stretch_image(im):
    """ Linearly stretch image to [0, 1] """
    return (im - im.min()) / (im.max() - im.min())


################################ FROM ex2!

from scipy.signal import convolve2d

def blur_spatial(im, kernel_size):
    """
    Blur image in spatial domain by convolution with gaussian kernel
    """
    return convolve2d(im, make_1d_gaussian(kernel_size), mode='same')
